import React, { createContext, useState } from "react";
import { LightTheme } from "./Theme"

export const ThemeContext = createContext();

export const ThemeProvider = (props) => {

    const [theme, setTheme] = useState(LightTheme);

    return (
        <ThemeContext.Provider value={[theme, setTheme]}>
            {props.children}
        </ThemeContext.Provider>
    );
};
