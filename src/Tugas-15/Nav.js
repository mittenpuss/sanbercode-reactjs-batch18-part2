import React,{useContext, useState,useEffect} from "react";
import {Link} from "react-router-dom";
import { ThemeContext } from "./ThemeContext";
import {DarkTheme, LightTheme} from "./Theme"

const Nav = () => {
    const [theme,setTheme] = useContext (ThemeContext)
    const [isDarkMode,setisDarkMode] = useState(false)

    useEffect(() => {
        setTheme(isDarkMode ? DarkTheme : LightTheme);
    }, [isDarkMode]);

  return (
    <div style={{
        backgroundColor: theme.colors.background,
        display:'flex',
        flexDirection:'row',
        justifyItems:'center',
        justifyContent:'space-evenly',
        alignItems:'center',
        height:'55px',
        borderBottom:theme.colors.borderBottom
        }}>

        <div>
            <Link to="/" style={theme.colors}>Home</Link>            
        </div>
        <div>
            <Link to="/tugas9" style={theme.colors}>Tugas 9</Link>
        </div>
        <div>
            <Link to="/tugas10" style={theme.colors}>Tugas 10</Link>
        </div>
        <div>
            <Link to="/tugas11" style={theme.colors}>Tugas 11</Link>
        </div>
        <div>
            <Link to="/tugas12" style={theme.colors}>Tugas 12</Link>
        </div>
        <div>
            <Link to="/tugas13" style={theme.colors}>Tugas 13</Link>
        </div>
        <div>
            <Link to="/tugas14" style={theme.colors}>Tugas 14</Link>
        </div>
        <div>
            <input type="checkbox" onClick={()=>setisDarkMode(!isDarkMode)}></input>
            <span style={theme.colors}>{isDarkMode?'Dark ':'Light '}Mode</span>
        </div>
        
    </div>
  )
}

export default Nav