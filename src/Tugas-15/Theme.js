export const DarkTheme = {
    colors: {
        background: "black",
        color:'white',
        textDecoration:'none'
    },
};

export const LightTheme = {
    colors: {
        background: "white",
        color:'black',
        textDecoration:'none',
        borderBottom:'1px solid gray'
    },
};
