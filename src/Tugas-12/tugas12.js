import React from "react"
import "../App.css"

class Tugas12 extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            dataHargaBuah: [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500},
            ]
        }
    }

    onDeleteClick=(index)=>{
        alert(index)
        
    }


render() {
    return (
        <div>
            <h1>Table Harga Buah</h1>
            <div className='Center'>
                
                <table>
                <thead>
                    <tr style={{backgroundColor:'darkgray'}}>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                {
                this.state.dataHargaBuah.map((item,index)=>{
                    return (    
                        <tr style={{backgroundColor:'coral'}} key={index}>
                            <td>{item.nama}</td>
                            <td>{item.harga}</td>
                            <td>{item.berat/1000} kg</td>
                            <td style={{textAlign:'center', display:'flex', flexDirection:'row',justifyContent:'center'}}>
                                <button>
                                    Edit
                                </button>
                                <button onClick={()=>this.onDeleteClick(index)}>
                                    Delete
                                </button>
                            </td>
                        </tr>                               
                    )
                })
                }
                </table>
            </div>

            <div style={{borderWidth:'1px', borderColor:'black', borderStyle:'solid', marginTop:'50px', marginLeft:'35%', marginRight:'35%', borderRadius:'15px'}}>
                <div style={{textAlign:'center'}}>
                    <h1>Form Pembelian Buah</h1>
                </div>
                
                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Nama Buah
                    </div>
                    <div style={{width:'50%'}}>              
                        <input type="text" name="name" />
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Harga Buah
                    </div>
                    <div style={{width:'50%'}}>              
                        <input type="text" name="name" />
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Berat Buah
                    </div>
                    <div style={{width:'50%'}}>              
                        <input type="text" name="name" />
                    </div>
                </div>
                
                <button style={{marginLeft:'20px',borderRadius:'20px', marginBottom:'30px'}}>
                    Kirim
                </button>
            
            </div>
        </div>
    );
  }

}

export default Tugas12