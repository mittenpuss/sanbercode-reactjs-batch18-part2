import React from "react"

class Tugas9 extends React.Component{

  render() {
    return (
        <div>
            <div style={{borderWidth:'1px', borderColor:'black', borderStyle:'solid', marginTop:'50px', marginLeft:'35%', marginRight:'35%', borderRadius:'15px'}}>
                
                <div style={{textAlign:'center'}}>
                    <h1>Form Pembelian Buah</h1>
                </div>
                
                <div style={{display:'flex', flexDirection:'row', width:'100%'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Nama Pelanggan
                    </div>
                    <div style={{width:'50%'}}>              
                        <input type="text" name="name" />
                    </div>
                </div>
                
                <div style={{display:'flex', flexDirection:'row', width:'100%',marginTop:'20px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Daftar Item
                    </div>
                    <div style={{width:'50%', display:'flex', flexDirection:'column'}}>
                        <div style={{display:'flex', flexDirection:'row'}}>
                        <input type="checkbox" name="pilihanBuah" />
                        <label>Semangka</label>                
                        </div>
                        <div style={{display:'flex', flexDirection:'row'}}>
                        <input type="checkbox" name="pilihanBuah" />
                        <label>Jeruk</label>                
                        </div>
                        <div style={{display:'flex', flexDirection:'row'}}>
                        <input type="checkbox" name="pilihanBuah" />
                        <label>Nanas</label>                
                        </div>
                        <div style={{display:'flex', flexDirection:'row'}}>
                        <input type="checkbox" name="pilihanBuah" />
                        <label>Salak</label>                
                        </div>
                        <div style={{display:'flex', flexDirection:'row'}}>
                        <input type="checkbox" name="pilihanBuah" />
                        <label>Anggur</label>                
                        </div>
                    </div>
                </div>
                
                <button style={{marginLeft:'20px',borderRadius:'20px', marginBottom:'30px'}}>
                    Kirim
                </button>
                
            </div>
        </div>
        
    );
  }

}

export default Tugas9




