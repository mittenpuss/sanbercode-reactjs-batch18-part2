import React from "react"
import "../App.css"

class IsiTable extends React.Component{
    render() {
        return (
        <tr style={{backgroundColor:'coral'}}>
            <td>{this.props.nama}</td>
            <td>{this.props.harga}</td>
            <td>{this.props.berat} kg</td>
        </tr>
        )

    }
}

class Tugas10 extends React.Component{

render() {
    let dataHargaBuah = [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
    ]

    return (
        <div>
            <h1>Table Harga Buah</h1>
            <div className='Center'>
                
                <table>
                <thead>
                    <tr style={{backgroundColor:'darkgray'}}>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Berat</th>
                    </tr>
                </thead>

                {
                dataHargaBuah.map((item)=>{
                    return (    
                    <IsiTable nama={item.nama} harga={item.harga} berat={item.berat/1000}/>                                
                    )
                })
                }

                </table>
            </div>
        </div>
    );
  }

}

export default Tugas10