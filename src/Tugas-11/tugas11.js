import React from "react"
import "../App.css"

class Tugas11 extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            time: 100,
            showTime: true,
            date: new Date()
        }
    }

    componentDidMount(){
        this.timerID = setInterval(
            () => this.tick(),
            1000
          );
    }

    tick(){
        this.setState(
            {
                time: this.state.time - 1,
                date: new Date()
            }
        )
    }

    componentDidUpdate(){
        if(this.state.time === 0)
        {
            this.state.showTime = false
        }
    }

    render() {
    return (
        <div>
            <div style={{textAlign:'center',fontSize:'30px',flexWrap:'wrap'}}>
                {
                this.state.showTime?
                    <div style={{display:'flex', flexDirection:'row', justifyContent:'space-evenly'}}>
                        <div>
                            <p>sekarang jam: {this.state.date.toLocaleTimeString()}</p>
                        </div>
                        <div>
                            <p>hitung mundur: {this.state.time}</p>
                        </div>
                    </div>
                :
                null
                }
            </div>
        </div>
    );
  }

}

export default Tugas11