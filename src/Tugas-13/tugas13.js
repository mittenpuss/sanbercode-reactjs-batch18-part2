import React,{useState,useEffect} from 'react'
import axios from 'axios'

const Tugas13=()=>{

    const [pesertaLomba, setPesertaLomba] =  useState(null)
    const [inputName,setInputName] = useState('')
    const [inputPrice,setInputPrice] = useState('')
    const [inputWeight,setInputWeight] = useState('')
    const [editID,setEditID] = useState(0)
    

    useEffect(() => {
        axios.get('http://backendexample.sanbercloud.com/api/fruits')
        .then((res)=>{
            setPesertaLomba(res.data)
        })
        .catch((err)=>{
            console.log(err)
        })
      },[]);


    const onDeleteClick=(id)=>{
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
        .then((res)=>{
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then((res)=>{
                setPesertaLomba(res.data)
                setInputName('')
                setInputPrice('')
                setInputWeight('')
            })
            .catch((err)=>{
            })
        })
        .catch((err)=>{
        })
    }

    const onEditClick=(id,name,price,weight)=>{
        setEditID(id)
        setInputName(name)
        setInputPrice(price)
        setInputWeight(weight)
    }

    const onSubmitClick=()=>{
        if(editID){
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${editID}`, {
                name: inputName,
                price: inputPrice,
                weight: inputWeight
            })
            .then((res)=>{
                console.log(res)
                setEditID(0)
                setInputName('')
                setInputPrice('')
                setInputWeight('')
                axios.get('http://backendexample.sanbercloud.com/api/fruits')
                .then((res)=>{
                    setPesertaLomba(res.data)
                })
                .catch((err)=>{
                    
                })

            })
            .catch((err)=>{
                console.log(err)
            })
        }else{

            console.log(inputName)
            console.log(inputPrice)
            console.log(inputWeight)
            
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {
                name: inputName,
                price: inputPrice,
                weight: inputWeight
            })
          .then(res => {
            console.log(res)
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then((res)=>{
                setPesertaLomba(res.data)
                setInputName('')
                setInputPrice('')
                setInputWeight('')
            })
            .catch((err)=>{
                
            })
          })
          .catch((err)=>{
              console.log(err)
          })
        }
        }

    return (
        <div>
            <h1>Table Harga Buah</h1>
            <div className='Center'>
                
                <table>
                <thead>
                    <tr style={{backgroundColor:'darkgray'}}>
                        <th>No</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Weight</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                {
                pesertaLomba && pesertaLomba.map((item,index)=>{
                    return (
                        <tbody key={index}>
                        <tr style={{backgroundColor:'coral'}}>
                            <td>{index+1}</td>
                            <td>{item.name}</td>
                            <td>{item.price}</td>
                            <td>{item.weight/1000} kg</td>
                            <td style={{textAlign:'center', display:'flex', flexDirection:'row',justifyContent:'center'}}>
                                <button onClick={()=>onEditClick(item.id,item.name,item.price,item.weight)}>
                                    Edit
                                </button>
                                <button onClick={()=>onDeleteClick(item.id)}>
                                    Delete
                                </button>
                            </td>
                        </tr>  
                        </tbody>                              
                    )
                })
                }
                </table>
            </div>

            <div style={{borderWidth:'1px', borderColor:'black', borderStyle:'solid', marginTop:'50px', marginLeft:'35%', marginRight:'35%', borderRadius:'15px'}}>
                <div style={{textAlign:'center'}}>
                    <h1>Form Pembelian Buah</h1>
                </div>
                
                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Nama Buah
                    </div>
                    <div style={{width:'50%'}}>              
                        <input type="text" name="name" value={inputName} defaultValue={inputName} onChange={e => setInputName(e.target.value)}/>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Harga Buah
                    </div>
                    <div style={{width:'50%'}}>              
                        <input type="text" name="name" value={inputPrice} defaultValue={inputPrice} onChange={e => setInputPrice(e.target.value)}/>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'row', width:'100%', paddingBottom:'15px'}}>
                    <div style={{paddingLeft:'20px',width:'30%'}}>
                        Berat Buah
                    </div>
                    <div style={{width:'50%'}}>              
                        <input type="number" name="name" value={inputWeight} defaultValue={inputWeight} onChange={e => setInputWeight(e.target.value)} />
                    </div>
                </div>
                
                <button onClick={()=> onSubmitClick()} style={{marginLeft:'20px',borderRadius:'20px', marginBottom:'30px'}}>
                    Kirim
                </button>
            
            </div>
        </div>
    )
}

export default Tugas13