import React from 'react';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './Tugas-15/Routes';
import Nav from './Tugas-15/Nav';
import { ThemeProvider } from './Tugas-15/ThemeContext';

function App() {
  return (
    <ThemeProvider>
      <Router>
        <div className="App">
          <Nav/>
          <Routes/>
        </div>
      </Router>   
    </ThemeProvider>
  );
}

export default App;
